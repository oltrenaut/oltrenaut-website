---
title: "Gioca con la radio"
summary: "Interactive installation"
categories: ["App development", "Game design"]
tags: ["post","lorem","ipsum"]
#externalUrl: ""
#showSummary: true
date: 2020-02-05
draft: false
---


## Project insights

An exhibit dedicated to the world of sound and radio: a full-immersion place based on a fun role game turning every girl and every boy into a deejay!

Play with the Radio, made with the Rai Radio Kids, promotes the development of children’s cognitive and emotional capacities, their ability to embody professional figures and face unknown situations in a reality-simulating playful context.
<b>Radio communication, using the most modern technologies, and sound, are combined to explore the potential of audio effects and your voice
</b>; you can listen back to your voice as many times as you wish and test the track chosen for release.

{{< figure src="01.jpg" title="Radio_01" >}}

<b>The station can also be used for deaf children thanks to the provision of a magnetic induction ring </b> that allows listening to children with cochlear implants.

{{< figure src="02.jpg" title="Radio_02" >}}

<span style="font-size:80%">
Made with the support of <a href="https://www.raiplaysound.it/radiokids"><b>Rai Radio Kids</b></a> <br>
Archive photos: Explora</span>


