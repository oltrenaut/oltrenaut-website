---
title: "Villa dei Quintili Adventure"
summary: "Education Children videogame"
categories: ["Game development", "Game design"]
tags: ["post","lorem","ipsum"]
#externalUrl: ""
#showSummary: true
date: 2022-07-05
draft: false
---


## Project insights

Villa dei Quintili Adventure is a videogame for children developed by using Minecraft tools and features. 
The story of the game takes place inside Villa dei Quintili, a huge archeological site of a roman villa where the player will meet <b>key figures linked to the story of the villa</b>, (the emperor Commodus, the gladiator Narcissus and more). 
The videogame features the reconstruction of the <b>whole archeological area</b> styled accordingly to Minecraft block design.
The adventure game design is the result of the delicate balance between entertainment and learning.


{{< vimeo 756722740 >}}

<i>Official teaser</i>


## Project development

After the investigation phase, understanding Minecraft workflow we started by bulding the world. Tha main challenge was to 
recreate an accurate but concise reproduction of the Roman Villa. <!--It has been important <b>to highlight the peculiar features of the site but also make an efficient and playable level design</b>.-->
We have been supported and guided by the archeological team of Villa dei Quintili.

<!--immagine della villa-->
{{< figure src="01.png" title="Villa dei Quintili_01" >}}

While designing the environment <b>we set a main storyline in collaboration with Explora team</b>. 

<!--immagine del dottore che ti saluta-->
{{< figure src="02.png" title="Villa dei Quintili_02" >}}

The charachters the player meet will give various objectives and tasks to the player covering <b>battles, puzzles, time trials, dungeons and scavenger hunts that are intented to convey historical facts</b> about the villa and its inhabitants.

<!--screens dei giochi-->
{{< figure src="03.png" title="Villa dei Quintili_03" >}}
<!--{{< figure src="04.png" title="Villa dei Quintili_04" >}}-->




