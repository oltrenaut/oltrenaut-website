---
title: "Space Travelers"
summary: "Augmented reality theatre play"
categories: ["Motion capture", "3D art", "Live coding"]
tags: ["post","lorem","ipsum"]
#externalUrl: ""
#showSummary: true
date: 2019-09-05
draft: false
---


## Project insights

A <b>performer controls in real time</b> (thanks to the use of sensors) the <b>movements of the animated character protagonist of the show</b>.
The 3D virtual world in which the narration is articulated was built with TouchDesigner, receiving signals from motion sensors through the OSC network protocol. Using the same protocol, a tablet is used for controlling the 3D scene in real time (camera, lights, scene change, events, audio triggers, etc.) and a second computer for the music and audio management (with Ableton Live and the use of its API through Max4Live).

{{< figure src="01.jpg" title="Space Traveller_01" >}}

{{< figure src="02.jpg" title="Space Traveller_02" >}}

{{< figure src="03.jpg" title="Space Traveller_03" >}}

<span style="font-size:80%">
Space Travellers has been commisioned by <a href="https://www.codemotion.com/"><b>Codemotion</b></a>  and performed at <a href="https://www.mast.org/"><b>Fondazione MAST</b></a> (Bologna, Italy)
</span>


