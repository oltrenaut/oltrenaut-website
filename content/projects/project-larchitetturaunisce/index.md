---
title: "L'architettura unisce"
summary: "Web design and web development"
categories: ["Web development", "Web design"]
tags: ["post","lorem","ipsum"]
#externalUrl: ""
#showSummary: true
date: 2021-02-05
draft: false
---


## Project insights

We developed the promotional website of an architects team for the 2021 elections of the CNAPPC (<b>National comitte of architects, urban planners, landscape designers and curators</b>).

{{< figure src="01.png" title="L'architettura unisce_01" >}}

The goal of the website was to introduce the values of the team, its plan and agenda and its members.
We tried to make <b>clear and accessible at glance</b> the broad and complex subject using points, key words and captivating and iconic illustrations.

{{< figure src="02.png" title="L'architettura unisce_02" >}}

{{< figure src="03.png" title="L'architettura unisce_03" >}}

<span style="font-size:80%">
Brand idenitity: G. Mazzei<br>
Illustrations: D. Goffredo<br>
<b>The website was needed just for the election time, now is offline</b>.
</span>


