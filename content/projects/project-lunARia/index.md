---
title: "LunARia"
summary: "Augmented reality exhibition"
categories: ["AR", "Game design"]
tags: ["post","lorem","ipsum"]
date: 2019-04-04
draft: false
---


## Project insights


Did you know that the director of the software division that put the man on the moon is a woman? Where does astronaut urine go? Who are Tardigrades? Curiosities about the history of lunar missions come to life with LunARia, augmented reality exhibition developed for <a href="https://makerfairerome.eu/en/"> <b>Maker Faire Rome -The European Edition</b></a>

{{< figure src="01.jpg" title="Lunaria_01" >}}

<span style="font-size:80%">
In collaboration with <a href="https://codemotionkids.com/"><b>Codemotion Kids</b></a>

Archive photos: Codemotion Kids</span>




