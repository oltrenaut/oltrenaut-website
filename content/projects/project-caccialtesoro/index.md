---
title: "Caccia al tesoro"
summary: "Augmented reality game"
categories: ["Web development","Game design"]
tags: ["tags"]
#externalUrl: ""
#showSummary: true
date: 2021-12-05
draft: false
---


## Project insights

"Caccia al tesoro" is a scavenger hunt to be played in through any device that involves an augmente reality quiz inside the gardens of Explora.
<b>The players find QR codes in the site that opens different challenges</b> with the aim of finding a secret ingredient for the experiment conducted by the two digial guides of the player: Stefania and Emanuele. Every correct answer will give an hint about where to find the precious ingredient.

{{< figure src="01.jpg" title="Caccia al tesoro_01" >}}

The activity allows to <b>approach playfully the STEM methodology</b> and to know fun facts about sciene, technology and engineering combining physical and digital gaming.

{{< figure src="02.jpg" title="Caccia al tesoro_01" >}}

<span style="font-size:80%">
Archive photos and video: Explora
</span>



