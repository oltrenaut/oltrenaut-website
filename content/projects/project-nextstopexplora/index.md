---
title: "Next stop Explora"
summary: "Simulation game re-design"
categories: ["Software development"]
tags: ["post","lorem","ipsum"]
#externalUrl: ""
#showSummary: true
date: 2021-09-05
draft: false
---


## Project insights

From the driver cab, the player will be able to operate a scale model of the new Frecciarossa 1000 train, which will travel at 2.40 m from the ground along a 50-metre track around the Explora fountain. The scale model of the new supertrain from Ferrovie dello Stato <b>provides children with an opportunity to play and at the same time understand topics such as environmental impact</b>, technological innovation, mobility and safety.

{{< youtube rer4NLWaNYU >}}

<i>Official teaser</i>

<span style="font-size:80%">
In collaboration with Ferrovie dello Stato Italiane, Trenitalia and Rete Ferroviaria Italiana<br>
Archive photos and video: Explora
</span>


