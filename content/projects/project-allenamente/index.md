---
title: "Allenamente"
summary: "2D videogames for medical research purposes"
categories: ["Game development","Game design","Backend development"]
tags: ["Lorem"]
#externalUrl: ""
#showSummary: true
date: 2022-05-05
draft: false
---


## Project insights

<!--logo-->


Allenamente is a children videogame developed for <b>Policlinico di Roma</b>. Its distinctive feature is its clinical use: the videogame it’s meant to be played by children affected by brain cancer and to <b>collect and track useful data about young patient status</b>.


The game features different mini-games and puzzles intended to provide help to a fictional scientists team that is attempting to evolve some cute little creatures. When the player wins the game, <b>he’s rewarded with some points that will be used to evolve the creatures, that will mutate in different shapes</b>.

<!--intro-->
{{< figure src="02.png" title="Allenamente_01" >}}


<b>The games are designed to be meaningful and useful to collect certain informations about childrens’ cognitive functions</b> such as sight capability, language, images and context understanding, memory and more. Medical staff got access to updated personal and peculiar medical records of every player.

Medical staff has also the control over different features of the games (from overall difficulty to more peculiar variables) in order to adjust the play experience for every patient.

<!--companion-->
{{< figure src="03.png" title="Allenamente_02">}}




## Minigames
<b>Puzzle</b>

{{< figure src="puzzle.png" title="Allenamente_01" >}}

<b>Quiz</b>

{{< figure src="quiz.png" title="Allenamente_01" >}}

<b>Scavenger hunt</b>

{{< figure src="hunt.png" title="Allenamente_01" >}}

<b>Crazy circuits</b>: a simon says with the goal to connect elements in sequence

{{< figure src="circuitis.png" title="Allenamente_01" >}}


